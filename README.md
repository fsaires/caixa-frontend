# Aplicação Desafio Caixa Frontend

Esta é uma aplicação básica em React configurada com Vite, um bundler extremamente rápido e flexível para projetos front-end.

## Pré-requisitos

Antes de começar, certifique-se de ter o seguinte instalado em sua máquina:

- [Node.js](https://nodejs.org/) (versão 12 ou superior)
- npm (normalmente é instalado junto com o Node.js)

## Instalação

1. Clone este repositório para sua máquina local usando:

```bash
git clone https://github.com/fsaires/caixa-frontend.git
```

2. Navegue até o diretório recém-clonado:
```bash
cd caixa-frontend
```

3. Instale as dependências do projeto:
```bash
npm install
```

4. Executando a aplicação:
```bash
npm run dev
```

5. Acessar a aplicação em desenvolvimento: 
[http://localhost:3000](http://localhost:3000)