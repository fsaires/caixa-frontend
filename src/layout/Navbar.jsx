import { AppBar, Typography, Toolbar, Link, Grid } from '@mui/material';
import styled from 'styled-components';

const StyledToolbar = styled(Toolbar)`
  align-items: center;
  font-size: 35px;
  margin: 7px;
`;

const Navbar = () => {
  return (
    <AppBar position="absolute">
      <StyledToolbar>
        <Grid container alignItems="center">
          <Grid item xs={4} sm={2}>
            <img src="/images/caixa.png" alt="CAIXA" style={{ maxWidth: '100%', height: 'auto' }} />
          </Grid>
          <Grid item xs={8} sm={6} md={6} justifyContent="flex-start" container>
            <Link href="/" underline="none" color="inherit">
              <Typography variant="h6">Home</Typography>
            </Link>
            <Link href="/galeria" underline="none" color="inherit" sx={{ marginLeft: 5 }}>
              <Typography variant="h6">Galeria de Fotos</Typography>
            </Link>
          </Grid>
          <Grid item xs={12} sm={4} md={4} container justifyContent="flex-end">
            <Typography variant="h6">Desafio Caixa Frontend</Typography>
          </Grid>
        </Grid>
      </StyledToolbar>
    </AppBar>
  );
};

export default Navbar;

