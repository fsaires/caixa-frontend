import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Toolbar, Typography } from '@mui/material';

const StyledFooter= styled(Toolbar)`
  height: 125px;
  bottom: -10px;
  background-color: #eff4f5;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
`;

const Footer = ({ author }) => {
  return (
      <StyledFooter>
        <Typography variant="h5" color="primary">
          &copy; 2024 Desafio Caixa Frontend | Desenvolvido por {author}
        </Typography>
      </StyledFooter>
  );
};

Footer.propTypes = {
  author: PropTypes.string.isRequired,
};

export default Footer;

