import PropTypes from 'prop-types';
import { Paper, Typography } from '@mui/material';
import { styled } from '@mui/system';

const StyledPaper = styled(Paper)`
  margin-top: 75px;
  margin-bottom: 25px;
  padding: 10px;
  border-radius: 7px;
`;

const PageTitle = ({ title }) => {
  return (
    <StyledPaper elevation={3}>
      <Typography variant="h4" align="center">
        {title}
      </Typography>
    </StyledPaper >
  );
};

PageTitle.propTypes = {
  title: PropTypes.string.isRequired,
};

export default PageTitle;