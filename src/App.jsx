import AppRoutes from './routes';
import { ThemeProvider } from '@mui/material/styles';
import { Container } from '@mui/material';

import Grid from '@mui/material/Grid';
import theme from './theme';
import Footer from './layout/Footer';
import Navbar from './layout/Navbar';

const App = () => {
  return (
    <>
      <ThemeProvider theme={theme}>
        <Container>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Navbar />
            </Grid>
            <Grid item xs={12}>
                <AppRoutes />
            </Grid>
            <Grid item xs={12}>
            </Grid>
          </Grid>
        </Container>
        <Footer author="Fernando Aires" />
      </ThemeProvider>
    </>
  );
};

export default App;

