import axios from 'axios';

const api = axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com/',
  headers: {
    'Content-Type': 'application/json',
  },
});

export const fetchAlbums = async () => {
  try {
    const response = await api.get('/photos');
    return response.data;
  } catch (error) {
    console.error('Erro ao consumir a API:', error);
    throw error;
  }
};