import { createTheme } from '@mui/material/styles';

const theme = createTheme ({
    palette: {
        primary: {
          main: '#0066B3', // Altere as cores conforme necessário
        },
        secondary: {
          main: '#f9fbfb', // Altere as cores conforme necessário
        },
      }
});

export default theme;