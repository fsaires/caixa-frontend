import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Home from './pages/Home';
import Album from './pages/Album';
import Galeria from './pages/Galeria';

const AppRoutes = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Home/>} />
        <Route path="/galeria" element={<Galeria/>} />
        <Route path="/album/:albumId" element={<Album/>} />
      </Routes>
    </Router>
  );
};

export default AppRoutes;
