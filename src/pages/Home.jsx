import { useState, useEffect } from 'react';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, IconButton, TablePagination } from '@mui/material';
import { Search as SearchIcon } from '@mui/icons-material';
import { Link } from 'react-router-dom';
import { fetchAlbums } from '../services/albumService';
import PageTitle from '../layout/PageTitle';

const ListaAlbum = () => {
  const [albums, setAlbums] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [distinctAlbumIds, setDistinctAlbumIds] = useState([]);

  useEffect(() => {
    const fetchAlbumData = async () => {
      try {
        const albumsData = await fetchAlbums();
        setAlbums(albumsData);
      } catch (error) {
        console.error('Error fetching albums:', error);
      }
    };

    fetchAlbumData();
  }, []);

  useEffect(() => {
    const distinctIds = [...new Set(albums.map(album => album.albumId))];
    setDistinctAlbumIds(distinctIds);
  }, [albums]);

  // Função para calcular a quantidade de IDs por álbum
  const calcularQuantidadeFotos = (albumId) => {
    const fotosDoAlbum = albums.filter(album => album.albumId === albumId);
    const idsDistintos = new Set();
    
    fotosDoAlbum.forEach(foto => {
      idsDistintos.add(foto.id);
    });
  
    return idsDistintos.size;
  };
  
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <>
      <PageTitle title="Lista de Álbuns" />
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell color="primary">Album ID</TableCell> {/* Use o estilo inline para aplicar a cor primária */}
              <TableCell color="primary">Album Nome</TableCell>
              <TableCell color="primary">Quantidade de Fotos</TableCell>
              <TableCell color="primary">Ação</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {(rowsPerPage > 0
              ? distinctAlbumIds.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              : distinctAlbumIds
            ).map((albumId) => (
              <TableRow key={albumId}>
                <TableCell>{albumId}</TableCell>
                <TableCell>{`Album ${albumId}`}</TableCell>
                <TableCell>{calcularQuantidadeFotos(albumId)}</TableCell>
                <TableCell>
                  {/* Use o componente Link para redirecionar para a página de detalhes */}
                  <Link to={`/album/${albumId}`}>
                    <IconButton>
                      <SearchIcon />
                    </IconButton>
                  </Link>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        labelRowsPerPage="Linhas por página:"
        rowsPerPageOptions={[10, 25, 50]}
        component="div"
        count={distinctAlbumIds.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </>
  );
};

export default ListaAlbum;
