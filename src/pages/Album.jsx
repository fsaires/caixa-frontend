import { useParams } from 'react-router-dom';
import { ImageList, ImageListItem, ImageListItemBar, IconButton } from '@mui/material';
import { fetchAlbums } from '../services/albumService';
import { useEffect, useState } from 'react';
import PageTitle from '../layout/PageTitle';

const Album = () => {
  const { albumId } = useParams();
  const [photos, setPhotos] = useState([]);

  useEffect(() => {
    const fetchPhotos = async () => {
      try {
        const response = await fetchAlbums();
        const photosOfAlbum = response.filter(photo => photo.albumId.toString() === albumId);
        setPhotos(photosOfAlbum);
      } catch (error) {
        console.error('Erro ao buscar as fotos do álbum:', error);
      }
    };

    fetchPhotos();
  }, [albumId]);

  return (
    <>
      <PageTitle title={`Fotos do Álbum ${albumId}`} />
      <ImageList sx={{ width: '100%' }} cols={3}>
        {photos.map((photo) => (
          <ImageListItem key={photo.id}>
            <img src={photo.thumbnailUrl} alt={photo.title} />
            <ImageListItemBar
              title={photo.title}
              actionIcon={
                <IconButton
                  sx={{ color: 'rgba(255, 255, 255, 0.54)' }}
                  aria-label={`info about ${photo.title}`}
                >
                </IconButton>
              }
            />
          </ImageListItem>
        ))}
      </ImageList>
    </>
  );
};

export default Album;
