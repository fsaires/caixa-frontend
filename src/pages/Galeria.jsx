import { useState, useEffect } from 'react';
import { Grid, Card, CardHeader, CardContent, CardMedia, IconButton, Typography } from '@mui/material';
import { fetchAlbums } from '../services/albumService';
import PageTitle from '../layout/PageTitle';
import Pesquisa from '../components/Pesquisa/Pesquisa';
import Paginacao from '../components/Paginacao/Paginacao';
import ModalGaleria from '../components/Modal/ModalGaleria';
import ZoomInIcon from '@mui/icons-material/ZoomIn';

const Galeria = () => {
  const [albums, setAlbums] = useState([]);
  const [page, setPage] = useState(1);
  const [searchQuery, setSearchQuery] = useState('');
  const [filteredAlbums, setFilteredAlbums] = useState([]);
  const [showZoomIcon, setShowZoomIcon] = useState(null);
  const [modalOpen, setModalOpen] = useState(false);
  const [selectedPhoto, setSelectedPhoto] = useState(null);
  const [photoLoading, setPhotoLoading] = useState(false);
  const pageSize = 20; 

  useEffect(() => {
    const fetchData = async () => {
      try {
        const albumsData = await fetchAlbums();
        setAlbums(albumsData);
      } catch (error) {
        console.error('Error fetching albums:', error);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    const filtered = albums.filter(album =>
      album.title.toLowerCase().includes(searchQuery.toLowerCase())
    );
    setFilteredAlbums(filtered);
  }, [albums, searchQuery]);

  const handlePageChange = (value) => {
    setPage(value);
  };

  const handleZoomButtonClick = (photo) => {
    setSelectedPhoto(photo);
    setPhotoLoading(true); 
    setModalOpen(true);
  };

  const handleCloseModal = () => {
    setSelectedPhoto(null);
    setModalOpen(false);
  };

  return (
    <>
      <PageTitle title="Galeria de Fotos" />
      <Pesquisa value={searchQuery} onChange={(e) => setSearchQuery(e.target.value)} />
      <Typography variant="h4" color="primary" style={{ margin: '20px 0' }}>
        Resultados da pesquisa:
      </Typography>
      {filteredAlbums.length === 0 ? (
        <Grid container spacing={3} style={{ display: 'flex', flexDirection: 'column', minHeight: '55vh' }}>
          <Grid item xs={12}>
            <Typography variant="h4" style={{ textAlign: 'center' }}>
              Nenhuma foto encontrada.
            </Typography>
          </Grid>
        </Grid>
      ) : (
        <>
          <Grid container spacing={3}>
            {filteredAlbums.slice((page - 1) * pageSize, page * pageSize).map(photo => (
              <Grid item xs={12} sm={6} md={4} lg={3} key={photo.id}>
                <Card
                  style={{ height: 425 }}
                  onMouseEnter={() => setShowZoomIcon(photo.id)}
                  onMouseLeave={() => setShowZoomIcon(null)}
                >
                  <CardHeader
                    title={photo.title}
                    style={{ height: 100 }}
                  />
                  <CardContent style={{ position: 'relative' }}>
                    <div style={{ position: 'relative', width: '100%', height: 250 }}>
                      <CardMedia
                        component="img"
                        height={250}
                        image={photo.thumbnailUrl}
                        alt={photo.title}
                      />
                      {showZoomIcon === photo.id && (
                        <IconButton
                          style={{
                            position: 'absolute',
                            top: '50%',
                            left: '50%',
                            transform: 'translate(-50%, -50%)',
                          }}
                          onClick={() => handleZoomButtonClick(photo)}
                        >
                          <ZoomInIcon style={{ fontSize: 75 }} />
                        </IconButton>
                      )}
                    </div>
                  </CardContent>
                </Card>
              </Grid>
            ))}
          </Grid>
          <Paginacao
            component="div"
            pageCount={Math.ceil(filteredAlbums.length / pageSize)}
            currentPage={page}
            onPageChange={handlePageChange}
            fotosPorPagina={pageSize}
            totalFotos={filteredAlbums.length}
          />
        </>
      )}
      <ModalGaleria 
        open={modalOpen} 
        onClose={handleCloseModal} 
        selectedPhoto={selectedPhoto} 
        photoLoading={photoLoading} 
        handleCloseModal={handleCloseModal} 
      />
    </>
  );
};

export default Galeria;