import PropTypes from 'prop-types';
import { Modal, CardMedia, Typography, Button } from '@mui/material';
import styled from 'styled-components';

const StyledModal = styled(Modal)`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const ContentWrapper = styled.div`
  background-color: white;
  padding: 20px;
  max-width: 90%;
  max-height: 90%;
  overflow: auto;
`;

const ModalGaleria = ({ open, onClose, selectedPhoto, handleCloseModal }) => {
  return (
    <StyledModal
      open={open}
      onClose={onClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <ContentWrapper>
        {selectedPhoto ? (
          <>
            <CardMedia
              component="img"
              height="auto"
              width="600"
              image={selectedPhoto.url}
              alt={selectedPhoto.title}
            />
            <Typography variant="h6" align="center" gutterBottom>
              {selectedPhoto.title}
            </Typography>
            <div style={{ textAlign: 'center', marginTop: 20 }}>
              <Button variant="contained" onClick={handleCloseModal}>Fechar</Button>
            </div>
          </>
        ) : (
          <Typography variant="h4" align="center">Nenhuma foto a ser exibida</Typography>
        )}
      </ContentWrapper>
    </StyledModal>
  );
};

ModalGaleria.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  selectedPhoto: PropTypes.object,
  handleCloseModal: PropTypes.func.isRequired,
};

export default ModalGaleria;
