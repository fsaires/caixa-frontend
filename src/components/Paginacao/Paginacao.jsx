import PropTypes from 'prop-types';
import { Pagination, Typography } from '@mui/material';
import styled from 'styled-components';

const StyledPaginacaoWrapper = styled.div`
  margin: auto;
  margin-top: 30px;
  text-align: center;
`;

const Paginacao = ({ pageCount, currentPage, onPageChange, fotosPorPagina, totalFotos }) => {
  const handlePageChange = (event, value) => {
    onPageChange(value);
  };

  // Calcular o total de fotos já vistas
  const totalFotosVistas = currentPage <= 1 ? 1 : (currentPage - 1) * fotosPorPagina + 1;
  const fotosFinais = Math.min(currentPage * fotosPorPagina, totalFotos);
  const totalFotosExibicao = `${totalFotosVistas} - ${fotosFinais} de ${totalFotos} fotos`;

  return (
    <div>
      <StyledPaginacaoWrapper>
        <Typography variant="h6">
          {totalFotosExibicao}
        </Typography> 
        <Pagination
          count={pageCount}
          page={currentPage}
          onChange={handlePageChange}
          color="primary"
          variant="outlined"
          shape="rounded"
          size="large"
        />
      </StyledPaginacaoWrapper>

    </div>
  );
};

Paginacao.propTypes = {
  pageCount: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  fotosPorPagina: PropTypes.number.isRequired,
  totalFotos: PropTypes.number.isRequired,
};

export default Paginacao;
