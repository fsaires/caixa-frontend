import PropTypes from 'prop-types';
import { TextField } from '@mui/material';
import styled from 'styled-components';

const StyledTextField= styled(TextField)`
  width: 50%;
  text-align: center;
`;

const Pesquisa = ({ value, onChange }) => {
  return (
    <StyledTextField
      id="pesquisa"
      label="Faça sua Pesquisa"
      value={value}
      onChange={onChange}
      type="search"
      size="small"
    />
  );
};

Pesquisa.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default Pesquisa;
